package com.my;

import com.api.safe.AlgorithmType;
import com.api.safe.coder.ICoder;
import com.api.safe.coder.ICoderFactory;
import com.api.safe.coder.impl.CoderFactory;
import com.api.safe.key.IKeyInfoFactory;
import com.api.safe.key.IkeyInfo;
import com.api.safe.key.impl.KeyInfoFactory;
import com.api.safe.util.SeedProducer;

public class TestCoder {
	public static void main(String[] args) {
		ICoderFactory coderFactory = new CoderFactory();
		ICoder coder = coderFactory.create(AlgorithmType.RSA);
		IKeyInfoFactory keyInfoFactory = new KeyInfoFactory();
		IkeyInfo keyInfo = keyInfoFactory.create(AlgorithmType.RSA);
		try {
			keyInfo.createKey(SeedProducer.getSeed());
			String data = coder.encode("123654", keyInfo.getPublicKey());
			data = coder.decode(data, keyInfo.getPrivateKey());
			System.out.println(data);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
