package com.api.safe.key.impl;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;

import com.api.safe.AlgorithmType;
import com.api.safe.util.Base64;

class RSAKeyInfo extends BaseKeyInfo {
	private static final int KEY_SIZE = 1024;

	@Override
	public String getPublicKey() {
		return mPublicKey;
	}

	@Override
	public String getPrivateKey() {
		return mPrivateKey;
	}

	@Override
	public void createKey(String seed) throws Exception {
		getSecretKey(seed, AlgorithmType.RSA.getAlgorithm());
	}

	private void getSecretKey(String seed, String algorithm) throws Exception {
		KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance(algorithm);
		SecureRandom secureRandom;
		if (seed != null && !"".equals(seed)) {
			secureRandom = new SecureRandom(seed.getBytes());
		} else {
			secureRandom = new SecureRandom();
		}
		keyGenerator.initialize(KEY_SIZE, secureRandom);
		KeyPair keyPair = keyGenerator.generateKeyPair();
		PrivateKey privateKey = keyPair.getPrivate();
		PublicKey publicKey = keyPair.getPublic();
		byte[] privateKeyData = Base64.encode(privateKey.getEncoded(), Base64.DEFAULT);
		mPrivateKey = new String(privateKeyData, CHARSET_UTF_8);
		byte[] publicKeyData = Base64.encode(publicKey.getEncoded(), Base64.DEFAULT);
		mPublicKey = new String(publicKeyData, CHARSET_UTF_8);
	}

}
