package com.api.safe.key.impl;

import com.api.safe.AlgorithmType;
import com.api.safe.key.IKeyInfoFactory;
import com.api.safe.key.IkeyInfo;

public class KeyInfoFactory implements IKeyInfoFactory {

	@Override
	public IkeyInfo create(AlgorithmType type) {
		switch (type) {
		case DES:
			return new DESKeyInfo();
		case DES3:
			return new DES3KeyInfo();
		case AES:
			return new AESKeyInfo();
		case RSA:
			return new RSAKeyInfo();
		}
		return null;
	}

}
