package com.api.safe.key.impl;

import java.security.Key;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import com.api.safe.AlgorithmType;
import com.api.safe.util.Base64;

class DESKeyInfo extends BaseKeyInfo {
	private static final int KEY_SIZE = 56;

	@Override
	public String getPublicKey() {
		return mPublicKey;
	}

	@Override
	public String getPrivateKey() {
		return mPrivateKey;
	}

	@Override
	public void createKey(String seed) throws Exception {
		byte[] secretKey = getSecretKey(seed, AlgorithmType.DES.getAlgorithm());
		Key k = createKey(secretKey, AlgorithmType.DES.getAlgorithm());
		byte[] raw = k.getEncoded();
		byte[] base64Data = Base64.encode(raw, Base64.DEFAULT);
		mPublicKey = mPrivateKey = new String(base64Data, CHARSET_UTF_8);
	}

	private static Key createKey(byte[] key, String algorithm) throws Exception {
		SecretKey secretKey = new SecretKeySpec(key, algorithm);
		return secretKey;
	}

	private static byte[] getSecretKey(String seed, String algorithm) throws Exception {
		KeyGenerator keyGenerator = KeyGenerator.getInstance(algorithm);
		SecureRandom secureRandom;
		if (seed != null && !"".equals(seed)) {
			secureRandom = new SecureRandom(seed.getBytes());
		} else {
			secureRandom = new SecureRandom();
		}
		keyGenerator.init(KEY_SIZE, secureRandom);
		SecretKey secretKey = keyGenerator.generateKey();
		return secretKey.getEncoded();
	}

}
