package com.api.safe.key.impl;

import com.api.safe.key.IkeyInfo;

public abstract class BaseKeyInfo implements IkeyInfo {
	protected static final String CHARSET_UTF_8 = "UTF-8";
	protected String mPublicKey;
	protected String mPrivateKey;
}
