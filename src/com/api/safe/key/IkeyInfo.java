package com.api.safe.key;

public interface IkeyInfo {
	public String getPublicKey();

	public String getPrivateKey();

	public void createKey(String seed) throws Exception;
}