package com.api.safe.key;

import com.api.safe.AlgorithmType;

public interface IKeyInfoFactory {

	public IkeyInfo create(AlgorithmType type);
}
