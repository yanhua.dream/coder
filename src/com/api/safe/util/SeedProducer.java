package com.api.safe.util;

import java.util.UUID;

public class SeedProducer {
	private static final int SEED_LENGTH = 24;
	private static final int SCALE_FACTOR = 1000;

	public static String getSeed() {
		String uuid = UUID.randomUUID().toString().replace("-", "");
		StringBuilder builder = new StringBuilder(uuid);
		int length = uuid.length();
		int removeLength = length - SEED_LENGTH;
		int removeIndex;
		int random;
		for (int i = 0; i < removeLength; i++) {
			random = (int) (SCALE_FACTOR * Math.random());
			removeIndex = random % (length - 1 - i);
			builder.deleteCharAt(removeIndex);
		}
		return builder.toString();
	}
}
