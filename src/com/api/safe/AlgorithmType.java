package com.api.safe;

public enum AlgorithmType {
	AES("AES", "AES/ECB/PKCS5Padding"), DES("DES", "DES/ECB/PKCS5Padding"), RSA("RSA", "RSA/ECB/PKCS1Padding"), DES3(
			"DESEDE", "DESEDE/ECB/PKCS5Padding");
	private String mAlgorithm;
	private String mMode;

	private AlgorithmType(String algorithm, String mode) {
		mAlgorithm = algorithm;
		mMode = mode;
	}

	public String getAlgorithm() {
		return mAlgorithm;
	}

	public String getMode() {
		return mMode;
	}
}