package com.api.safe.coder;

import com.api.safe.AlgorithmType;

public interface ICoderFactory {
	public ICoder create(AlgorithmType type);
}
