package com.api.safe.coder.impl;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

import com.api.safe.AlgorithmType;
import com.api.safe.coder.ICoder;
import com.api.safe.util.Base64;

public class RSACoder extends BaseCoder {
	private static final String CHARSET_UTF_8 = "UTF-8";

	private static final ICoder CODER = new RSACoder();

	private RSACoder() {

	}

	@Override
	public String encode(String data, String pubKey) throws Exception {
		byte[] raw = Base64.decode(pubKey.getBytes(CHARSET_UTF_8), Base64.DEFAULT);
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(raw);
		KeyFactory keyFactory = KeyFactory.getInstance(AlgorithmType.RSA.getAlgorithm());
		PublicKey publicKey = keyFactory.generatePublic(keySpec);

		Cipher cipher = Cipher.getInstance(AlgorithmType.RSA.getMode());
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		byte[] encodeData = cipher.doFinal(data.getBytes(CHARSET_UTF_8));
		byte[] base64Data = Base64.encode(encodeData, Base64.DEFAULT);
		return new String(base64Data, CHARSET_UTF_8);
	}

	@Override
	public String decode(String data, String priKey) throws Exception {
		byte[] raw = Base64.decode(priKey.getBytes(CHARSET_UTF_8), Base64.DEFAULT);
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(raw);
		KeyFactory keyFactory = KeyFactory.getInstance(AlgorithmType.RSA.getAlgorithm());
		PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
		Cipher cipher = Cipher.getInstance(AlgorithmType.RSA.getMode());
		cipher.init(Cipher.DECRYPT_MODE, privateKey);
		byte[] base64Data = Base64.decode(data.getBytes(CHARSET_UTF_8), Base64.DEFAULT);
		byte[] decodeData = cipher.doFinal(base64Data);
		return new String(decodeData, CHARSET_UTF_8);
	}

	public static ICoder getInstance() {
		return CODER;
	}
}
