package com.api.safe.coder.impl;

import com.api.safe.coder.ICoder;

public abstract class BaseCoder implements ICoder {

	@Override
	public abstract String encode(String data, String publicKey) throws Exception;

	@Override
	public abstract String decode(String data, String privateKey) throws Exception;

}
