package com.api.safe.coder.impl;

import com.api.safe.AlgorithmType;
import com.api.safe.coder.ICoder;
import com.api.safe.coder.ICoderFactory;

public class CoderFactory implements ICoderFactory {

	@Override
	public ICoder create(AlgorithmType type) {
		switch (type) {
		case DES:
			return DESCoder.getInstance();
		case DES3:
			return DES3Coder.getInstance();
		case AES:
			return AESCoder.getInstance();
		case RSA:
			return RSACoder.getInstance();
		}
		return null;
	}

}
