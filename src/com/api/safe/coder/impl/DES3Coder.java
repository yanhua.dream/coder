package com.api.safe.coder.impl;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import com.api.safe.AlgorithmType;
import com.api.safe.coder.ICoder;
import com.api.safe.util.Base64;

public class DES3Coder extends BaseCoder {
	private static final String CHARSET_UTF_8 = "UTF-8";

	private static final ICoder CODER = new DES3Coder();

	private DES3Coder() {

	}

	@Override
	public String encode(String data, String pubKey) throws Exception {
		byte[] raw = Base64.decode(pubKey.getBytes(CHARSET_UTF_8), Base64.DEFAULT);
		SecretKeySpec secretKeySpec = new SecretKeySpec(raw, AlgorithmType.DES3.getAlgorithm());
		Cipher cipher = Cipher.getInstance(AlgorithmType.DES3.getMode());
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
		byte[] encodeData = cipher.doFinal(data.getBytes(CHARSET_UTF_8));
		byte[] base64Data = Base64.encode(encodeData, Base64.DEFAULT);
		return new String(base64Data, CHARSET_UTF_8);
	}

	@Override
	public String decode(String data, String priKey) throws Exception {
		byte[] raw = Base64.decode(priKey.getBytes(CHARSET_UTF_8), Base64.DEFAULT);
		SecretKeySpec secretKeySpec = new SecretKeySpec(raw, AlgorithmType.DES3.getAlgorithm());
		Cipher cipher = Cipher.getInstance(AlgorithmType.DES3.getMode());
		cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
		byte[] base64Data = Base64.decode(data.getBytes(CHARSET_UTF_8), Base64.DEFAULT);
		byte[] decodeData = cipher.doFinal(base64Data);
		return new String(decodeData, CHARSET_UTF_8);
	}

	public static ICoder getInstance() {
		return CODER;
	}
}
