package com.api.safe.coder;

public interface ICoder {
	public String encode(String data, String publicKey) throws Exception;

	public String decode(String data, String privateKey) throws Exception;
}
